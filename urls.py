#!/usr/bin/env python
# coding=utf-8

"""URL routings."""

from django.urls import include, re_path

urlpatterns = (
    re_path(r"", include("django_otp.urls")),
)
